﻿## Snake
Snake game developed in JAVA.
When the user dies, the game resumes alone.

The .jar allows to run the game without compiling it in an IDE, it is located in the folder "dist".

Good game :)

Jeu du Snake développé en JAVA.
Quand l'utilisateur meurt, le jeu reprend tout seul.

Le .jar permet d'éxécuter le jeu sans le compiler dans un IDE, il se situe dans le dossier "dist".

Bon jeu :)
