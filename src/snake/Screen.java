/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

/**
 *
 * @author thibaudlambert
 */
public class Screen extends JPanel implements Runnable {

    //Attributs
    public static final int WIDTH = 500, HEIGHT = 500;
    private Thread thread;
    public static boolean running = false;
    private BodyPart b;
    private ArrayList<BodyPart> snake;
    private int xCoor = 10, yCoor = 10;
    private int size = 5;
    public static boolean right = true, left = false, up = false, down = false;
    private int ticks = 0;
    private Key key;
    private Apple apple;
    private ArrayList<Apple> apples;
    private Random r;
    public static final int INDICE = 20;

    //Constructeurs
    public Screen() {
        this.setFocusable(true);
        key = new Key();
        this.addKeyListener(key);
        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        snake = new ArrayList();
        apples = new ArrayList();
        r = new Random();
        start();
    }

    public void tick() {
        if (snake.size() == 0) {
            b = new BodyPart(xCoor, yCoor, INDICE);
            snake.add(b);
        }

        if (apples.size() == 0) {
            int xCoor = r.nextInt(WIDTH / INDICE);
            int yCoor = r.nextInt(HEIGHT / INDICE);
            apple = new Apple(xCoor, yCoor, INDICE);
            apples.add(apple);
        }

        for (int i = 0; i < apples.size(); i++) {
            if (xCoor == apples.get(i).getxCoor() && yCoor == apples.get(i).getyCoor()) {
                size++;
                apples.remove(0);
                i--;
            }
        }

        for (int i = 0; i < snake.size(); i++) {
            if (xCoor == snake.get(i).getxCoor() && yCoor == snake.get(i).getyCoor()) {
                if (i != snake.size() - 1) {
                    stop();
                }
            }
        }

        if (xCoor < 0 || xCoor > WIDTH / INDICE || yCoor < 0 || yCoor > HEIGHT / INDICE) {
            stop();
        }

        ticks++;

        if (ticks > 250000) {
            if (right) {
                xCoor++;
            }
            if (left) {
                xCoor--;
            }
            if (up) {
                yCoor--;
            }
            if (down) {
                yCoor++;
            }

            ticks = 0;
            b = new BodyPart(xCoor, yCoor, INDICE);
            snake.add(b);

            if (snake.size() > size) {
                snake.remove(0);
            }
        }
    }

    @Override
    public void paint(Graphics g) {
        g.clearRect(0, 0, WIDTH, HEIGHT);
        g.setColor(Color.BLACK);
        for (int i = 0; i < WIDTH / INDICE; i++) {
            g.drawLine(i * INDICE, 0, i * INDICE, HEIGHT);
        }

        for (int i = 0; i < HEIGHT / INDICE; i++) {
            g.drawLine(0, i * INDICE, WIDTH, i * INDICE);
        }

        for (int i = 0; i < snake.size(); i++) {
            snake.get(i).draw(g);
        }

        for (int i = 0; i < apples.size(); i++) {
            apples.get(i).draw(g);
        }
    }

    public void start() {
        running = true;
        thread = new Thread(this, "Game Loop");
        thread.start();
    }

    public void stop() {
        running = false;
        /*try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Screen.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }

    @Override
    public void run() {
        while (running) {
            tick();
            repaint();
        }
        right = true;
        left = false;
        up = false;
        down = false;
        snake.clear();
        apples.clear();
        size = 5;
        ticks = 0;
        xCoor = 10;
        yCoor = 10;
        repaint();
        thread.interrupt();
        start();
    }
}
