/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author thibaudlambert
 */
public class Key implements KeyListener {

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        
        if (key == KeyEvent.VK_RIGHT && !Screen.left) {
            Screen.up = false;
            Screen.down = false;
            Screen.right = true;
        }

        if (key == KeyEvent.VK_LEFT && !Screen.right) {
            Screen.up = false;
            Screen.down = false;
            Screen.left = true;
        }

        if (key == KeyEvent.VK_UP && !Screen.down) {
            Screen.left = false;
            Screen.right = false;
            Screen.up = true;
        }

        if (key == KeyEvent.VK_DOWN && !Screen.up) {
            Screen.left = false;
            Screen.right = false;
            Screen.down = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

}
