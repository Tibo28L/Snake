/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.GridLayout;
import javax.swing.JFrame;

/**
 *
 * @author thibaudlambert
 */
public class Frame extends JFrame {
    //Constructeurs
    public Frame() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Snake");
        this.setResizable(false);
        
        this.init();
    }
    
    public void init() {
        this.setLayout(new GridLayout(1,1,0,0));
        Screen s = new Screen();
        this.add(s);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}
